package com.movieguide.ph

object Config {
    const val androidMinSdk = 26
    const val androidAppId = "com.movieguide.ph"
    const val androidTargetSdk = 30
    const val androidVerCode = 1
    const val androidVerName = "1.0"
}
