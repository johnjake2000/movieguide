package com.movieguide.ph

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.movieguide.ph.data.sealed.State
import com.movieguide.ph.data.vo.MovieDetails
import com.movieguide.ph.databinding.ActivityMainBinding
import com.movieguide.ph.features.details.DetailsViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var uiStateJob: Job? = null
    private val viewModel: DetailsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel.getMovieDetails(505)
    }

    override fun onStart() {
        super.onStart()
        uiStateJob = lifecycleScope.launch {
            viewModel.stateDetails.collect { value ->
                handleDetails(value)
            }
        }
    }

    override fun onStop() {
        uiStateJob?.cancel()
        super.onStop()
    }

    private fun handleDetails(state: State<MovieDetails>) {
        when(state) {
            is State.Data -> handleSuccess(state.data)
            is State.Error -> handleFailed(state.error)
            else -> Timber.e("An error occurred")
        }
    }

    private fun handleFailed(error: Throwable) {
        Timber.d("error on ${error.message}")
    }

    private fun handleSuccess(movie: MovieDetails) {
        Timber.d("$movie")
        binding.txtInput.text = movie.original_title
    }
}