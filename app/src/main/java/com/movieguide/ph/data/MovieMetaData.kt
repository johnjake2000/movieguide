package com.movieguide.ph.data

import android.os.Parcelable
import com.movieguide.ph.data.vo.Dates
import com.movieguide.ph.data.vo.Movies
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieMetaData(
    val page: Int = 0,
    val total_results: Int = 0,
    val total_pages: Int = 0,
    val results: List<Movies>,
    val dates: Dates? = null
) : Parcelable
