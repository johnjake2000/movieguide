package com.movieguide.ph.features.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.movieguide.ph.data.sealed.State
import com.movieguide.ph.data.vo.MovieDetails
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class DetailsViewModel(
    private val repository: Repository
) : ViewModel() {

    private val movieDetailsState = MutableStateFlow<State<MovieDetails>>(State.Empty)

    val stateDetails: StateFlow<State<MovieDetails>> = movieDetailsState

    fun getMovieDetails(movieId: Int) {
        viewModelScope.launch {
            val data = repository.getMovieDetailsById(movieId)
            val dataState = State.Data(data)
            movieDetailsState.value = dataState
        }
    }
}