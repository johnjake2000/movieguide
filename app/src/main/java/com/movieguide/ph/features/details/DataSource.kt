package com.movieguide.ph.features.details

import com.movieguide.ph.data.vo.MovieDetails
import com.movieguide.ph.data.vo.Movies

interface DataSource {
    suspend fun getMovieDetailsById(movieId: Int): MovieDetails
    suspend fun getTopRatedMovieById(movieId: Int): Movies
    suspend fun getDiscoverMovieById(movieId: Int): Movies
    suspend fun insertMovie(movie: Movies)
}