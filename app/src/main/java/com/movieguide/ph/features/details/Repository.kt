package com.movieguide.ph.features.details

import com.movieguide.ph.BuildConfig
import com.movieguide.ph.api.ApiServices
import com.movieguide.ph.data.vo.MovieDetails
import com.movieguide.ph.data.vo.Movies
import com.movieguide.ph.di.providesApiKey
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject

class Repository(private val api: ApiServices): DataSource {

    private val apiKey: String = BuildConfig.API_KEY

    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Timber.e("Http Error ###: ${throwable.message}")
    }

    override suspend fun getMovieDetailsById(movieId: Int): MovieDetails {
        return coroutineScope.async(exceptionHandler) {
            return@async withContext(Dispatchers.IO) {
                val movieData = api.getMovieDetails(movieId, apiKey)
                movieData
            }
        }.await()
    }

    override suspend fun getTopRatedMovieById(movieId: Int): Movies {
        TODO("Not yet implemented")
    }

    override suspend fun getDiscoverMovieById(movieId: Int): Movies {
        TODO("Not yet implemented")
    }

    override suspend fun insertMovie(movie: Movies) {
        TODO("Not yet implemented")
    }
}