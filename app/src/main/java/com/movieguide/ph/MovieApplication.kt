package com.movieguide.ph

import android.app.Application
import androidx.paging.ExperimentalPagingApi
import com.movieguide.ph.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MovieApplication: Application() {
    @ExperimentalPagingApi
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MovieApplication)
            modules(listOf(
                networkModule,
                storageModule,
                repositoryModule,
                viewModelModule,
                appModule
                /**mapperModule,
                schedulerModule,
                databaseModule,
                viewModelModule,
                implementationModule,
                repositoryModule,
                mediatorModule**/
            ))
        }
    }
}