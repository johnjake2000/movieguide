package com.movieguide.ph.di

import com.movieguide.ph.features.details.DetailsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { DetailsViewModel(repository = get()) }
}