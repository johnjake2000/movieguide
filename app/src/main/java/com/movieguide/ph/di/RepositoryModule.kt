package com.movieguide.ph.di

import org.koin.dsl.module

val repositoryModule = module {
    factory { com.movieguide.ph.features.details.Repository(api = get()) }
}