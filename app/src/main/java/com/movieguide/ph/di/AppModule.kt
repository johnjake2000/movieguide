package com.movieguide.ph.di

import android.app.Application
import android.content.Context
import com.movieguide.ph.MovieApplication
import org.koin.dsl.module

val appModule = module {
    single { providesMovieApplication() }
}
fun providesApplicationContext(app: Application): Context {
    return app.applicationContext
}

fun providesMovieApplication(): MovieApplication {
    return MovieApplication()
}