package com.movieguide.ph.features.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.movieguide.ph.CoroutineTestRule
import com.movieguide.ph.data.sealed.State
import com.movieguide.ph.di.networkModule
import com.movieguide.ph.di.repositoryModule
import com.movieguide.ph.di.viewModelModule
import io.kotlintest.shouldBe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Rule
import org.junit.jupiter.api.*
import org.junit.rules.TestRule
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.testcontainers.junit.jupiter.Testcontainers

@Testcontainers
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExperimentalCoroutinesApi
class DetailsUnitTest: KoinTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val dispatcher = TestCoroutineDispatcher()

    private val movieId: Int = 505
    private val viewModel: DetailsViewModel by inject()

    @BeforeEach
    fun initialized() {
        /** for dependency injection **/
       startKoin {
           printLogger()
           modules(listOf(
               networkModule,
               repositoryModule,
               viewModelModule
           ))
       }
        Dispatchers.setMain(dispatcher)
   }

    @AfterEach
    fun endTest(){
      stopKoin()
      Dispatchers.resetMain()
    }

    @Test
    @Order(1)
    fun `initialized viewModel value`() = runBlocking {
         viewModel.getMovieDetails(movieId)
    }

    @Test
    @Order(2)
    fun `start data collect`() = runBlocking {
        viewModel.stateDetails.take(2).collect { result ->
            if(result is State.Data) {
                result.data.budget shouldBe 20000000
                result.data.title shouldBe "Johnny Handsome"
                println(result.data.title)
            }
        }
    }
}