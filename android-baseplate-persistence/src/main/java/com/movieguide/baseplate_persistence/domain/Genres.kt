package com.movieguide.baseplate_persistence.domain

data class Genres(
    val id : Int = 0,
    val name : String? = ""
)
